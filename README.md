## Tamu CTF 2020

* **CTF Time page:** https://ctftime.org/event/1009
* **Category:** Jeopardy
* **Date:** Fri, 20 March 2020, 00:30 UTC — Mon, 30 March 2020, 00:30 UTC

---

### Solved Challenges
| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
|angrmanagement              |rev       |188     |angr          |
|bbpwn                       |pwn       |50      |bof, gets()   |
|blind                       |misc      |50      |bash return codes|
|echo as a service           |pwn       |113     |format string vulnerability, printf()|
|rusty at reversing          |rev       |113     |static analysis|

### Unsolved Challenges to Revisit
| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
|about time                  |rev       |        |              |
|troll                       |pwn       |50      |bof, rand, srand|
|zippity doo dah             |steg      |        |zip, steg     |
