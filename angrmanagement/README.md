## angrmanagement

* **CTF:** Tamu CTF
* **Category:** rev
* **Points:** 188
* **Author(s):** b0uldrr

---

### Challenge
```
nc challenges.tamuctf.com 4322
```

### Downloads
* [angrmanagement](angrmanagement)

---

### Solution
_Note... the solution that I used to get the flag, presented below, works. But in reading another writeup after the event, I realise I approached this problem in the completely wrong way. Please read the writeup [here](https://insomn14.github.io/posts/tamuctf-2020/) where the author uses a tool named [angr](https://docs.angr.io/core-concepts/pathgroups) to work everything out automatically. Given the name of the challenge, this was clearly what they were intending participants to use. Don't be like me._

Running the provided binary, we are prompted for a password. With the incorrect password the program just exits.

![example](images/example.png)

I decompiled the binary in Ghidra. The main function prompts for the user password and then runs the the input through 32 separate `check` functions which test the individual characters of the input against a variety of rules. If the inputted password fails any of those checks then the program exits, but if it passes them all then the program loads and prints the flag.

Here's an example of 3 of he 32 check functions:
![check9](images/check9.png)
![check13](images/check13.png)
![check29](images/check29.png)

I went through all of them and made a spreadsheet with the criteria that each character had to meet. Most characters had about 3 rules that they needed to satisfy.
![spreadsheet](images/spreadsheet.png)

Doing this by hand took hours, but in the end I was able to work out a password that would satisfy all of the checks: `2#P2YGYmT[$D520adhaAa@{(i.cAAA/j` 

I stored this password in a text file, `password.txt` and then directed it into the challenge server which printed the flag:

```
cat password.txt | nc challenges.tamuctf.com 4322
Enter the password:
Correct!
gigem{4n63r_m4n463m3n7}
```
---

### Flag 
```
gigem{4n63r_m4n463m3n7}
```
