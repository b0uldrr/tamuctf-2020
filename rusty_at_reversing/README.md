## Rusty at Reversing

* **CTF:** Tamu CTF 2020
* **Category:** rev
* **Points:**  113
* **Author(s):** b0uldrr

---

### Challenge
```
A nice little reversing challenge for those who are a little rusty!
```

### Downloads
* [librusty_at_reversing.so](librusty_at_reversing.so)

---

### Solution
I tried to run the provided binary but it immediately ended with a segmentation fault. I decompiled it with Ghidra and found 3 interesting functions - `get_flag`, `encrypt` and `decrypt`.

We can see the `get_flag` function initialises an array with varying byte values and then passes the array to the decrypt function. We can assume that this array holds the flag, we just need to figure out how to decrypt it.

![get flag](images/get_flag.png)

The decrypt function loops through the array and does the following:
1. Stores the first array element's value in a temporary variable
1. XOR's the first array element with 0xE4
1. Replaces that first array element with the XOR output 
1. Changes the XOR value to the value stored in the temporary variable in step 1 and then loops to the next element

Here's a modified version of the Ghidra output, which I've cleaned up a little and removed any extraneous code:

![decrypt](images/decrypt_modified.png)

The `encrypt` function basically did the exact same thing.

I copied the encrypted flag value found in `get_flag` into a python script and recreated the algorithm found in decrypt:

```
#! /usr/bin/python3

# Our encoded flag values. Found in the get_flag function when I opened the binary in Ghidra
enc  = [0x83, 0xea, 0x8d, 0xe8, 0x85, 0xfe, 0x93, 0xe1, 0xbe, 0xcd, 0xb9, 0xd8, 0xaa, 0xc1, 0x9e, 0xf7, 0xa8, 0xce, 0xab, 0xce, 0xa2, 0xfd, 0x8f, 0xfa, 0x89, 0xfd, 0x84, 0xf9]

# Our initial key value. Found in the encrypt and decrypt functions.
key  = 0xe4

flag = ""

for c in enc:
    flag += chr(c ^ key)
    key = c 

print(flag)
```

Running this script prints the flag.

### Flag 
gigem{mr_stark_i_feel_rusty}
```
